# Ansible Role: Packer

## What does this role do?

- download and install packer if not available
- render packer template and validate
- run packer build with rendered template
