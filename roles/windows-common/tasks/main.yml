---
- wait_for_connection:
  tags: always

- setup:
  tags: always

- name: edit registry to show file extensions
  win_regedit:
    path: HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced
    name: HideFileExt
    type: dword
    data: 0
  tags:
    - show_file_ext

- name: edit registry to show hidden
  win_regedit:
    path: HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced
    name: Hidden
    type: dword
    data: 1
  tags:
    - show_hidden

- name: Set machine account password (reboot required)
  win_shell: "ksetup /SetComputerPassword {{ansible_password}}"

- name: run win_updates (could take 30 mins or more)
  tags: win_updates
  when: run_win_updates|bool
  win_updates:
    reboot: yes  # auto reboot if required
    log_path: C:\ansible_win_updates.log
    category_names:
      - CriticalUpdates
      - SecurityUpdates
      - UpdateRollups


- name: win_feature install features
  win_feature:
    name:
      - Net-Framework-Core
      - AD-Domain-Services
      - RSAT-AD-Tools
    include_sub_features: yes
    include_management_tools: yes
  register: win_feature_result
  tags:
    - win_feature

- name: win_reboot after win_feature if required
  win_reboot:
  when: win_feature_result.reboot_required

- name: win_chocolatey install packages with params
  win_chocolatey:
    name: "{{item.name}}"
    params: "{{item.params}}"
  loop:
    - name: "openssh"
      params: "'/SSHServerFeature'"
  tags:
    - win_chocolatey

- name: win_chocolatey install packages without params
  win_chocolatey:
    name:
      - firefox
      - cmder
      - gow
      - notepadplusplus
      - winpcap
      - everything
      - wireshark
      - git
      - vim
      - pswindowsupdate
      - python
  tags:
    - win_chocolatey

- name: win_chocolatey install packages as SYSTEM user
  become: yes
  become_user: SYSTEM
  become_method: runas
  win_chocolatey:
    name:
      # powershell 5
      - powershell
      - vcredist-all
      - wixtoolset
      - dotnet4.7.1
      - microsoft-message-analyzer
      - networkmonitor
  tags:
    - win_chocolatey

- name: check .NET Framework version via registry
  win_reg_stat:
    path: HKLM:\SOFTWARE\Microsoft\NET Framework Setup\NDP\v4\Full
    name: Release
  register: dotnet_release

- name: make sure .NET version is 4.7.1+
  fail:
    msg: "current .NET version: {{dotnet_release}}, minimal required: 4.7.1(461310)"
  when: dotnet_release.value < 461310

- name: config dns servers
  win_dns_client:
    adapter_names: "Ethernet"
    ipv4_addresses: "{{DNS_NAMESERVERS}}"
  when: DNS_NAMESERVERS is defined

# win_firewall requires Windows Management Framework 5 or higher.
# which means powershell 5
- name: disable firewall
  win_firewall:
    state: disabled
    profiles:
      - Domain
      - Private
      - Public
