# Ansible Samba Upgrade

Ansible Project to test Samba upgrade by join.


## How it works
Refer to `upgrade.yml` for example.
In this playbook, we setup 2 DCs: dc0 and dc1, and upgrade from v4-9-stable to master.

dc0 is the primay dc.
By adding it to group `v4-9-stable`, the vars defined in `group_vars/4-9-stable.yml` will apply to it by ansible,
which will set `samba_repo_version` to `v4-9-stable` to use old version for this dc.

dc1 is the backup dc with code on master, which will join to dc0 to finish the upgrade.

If you want to try different upgrade path, refer to v45-v49.yml for example.
