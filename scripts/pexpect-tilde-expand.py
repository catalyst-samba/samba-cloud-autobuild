#!/usr/bin/env python3
"""
Get tilde expand time with pexpect.

In Linux Bash terminal, when you type `~ Tab Tab`, it will try to expand all
available users on system.

On a Samba server, this feature was extended to also expand all domain users.
However, if there are too many users(e.g: 10k), the expand may take minutes to
finish, or even freeze.

This script can help to measure the expand time, so we can test this feature
and integrate with automation in a easy way.

To run it on localhost:

    ./pexpect-tidle-expand.py

To run it via ssh against a remote server:

    ./pexpect-tidle-expand.py --command "ssh ubuntu@IP"

Author: Joe Guo <joeg@catalyst.net.nz>
"""

import sys
import time
import json
import pexpect
import argparse


def parse_args():
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        description='Measure tilde expand(~ Tab Tab) time with pexpect.')

    parser.add_argument(
        '-c', '--command', default='/bin/bash',
        help='command to run, for localhost: /bin/bash, for remote: ssh ubuntu@IP')
    parser.add_argument(
        '-t', '--timeout', default=60, type=int,
        help='response timeout(seconds)')
    parser.add_argument(
        '-r', '--repeat', default=1, type=int,
        help='Repeat this many times to get average')
    parser.add_argument(
        '-v', '--verbose', action='store_true',
        help='Print verbose info')

    return parser.parse_args()


cli = parse_args()

if cli.verbose:
    def verbose(*args, **kwargs):
        kwargs['file'] = sys.stderr
        print(*args, **kwargs)
else:
    def verbose(*args, **kwargs):
        pass

verbose(cli)


def json_dumps(obj):
    return json.dumps(obj, indent=4, sort_keys=True)


def test_tilde_tab_tab(command, timeout):
    """
    Test ~ Tab Tab time.
    """
    with pexpect.spawn(command, timeout=timeout) as process:
        assert process.isalive()
        # wait for cli prompt after connect
        process.expect(r'$')

        start = time.time()
        process.sendline('~\t\t')
        # may finish with $ or ask:
        # Display all 5034 possibilities? (y or n)
        index = process.expect([r'y or n'])
        verbose(process.before.decode())
        if index == 0:
            process.sendline('y')
            index = process.expect([r'--More--'])
            if index == 0:
                process.sendline('q')
            verbose(process.before.decode())

    return time.time() - start


def main():
    spans = [test_tilde_tab_tab(cli.command, cli.timeout) for _ in range(cli.repeat)]
    verbose(spans)
    average = sum(spans) / cli.repeat
    result = {
        'command': cli.command,
        'repeat': cli.repeat,
        'average': '%.2f' % average,
    }
    print(json_dumps(result))


if __name__ == '__main__':
    main()
