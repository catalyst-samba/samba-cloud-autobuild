#!/usr/bin/python
"""Generate graphs from traffic runner summary files.

This shows the overall throughput of the traffic runner at various
loads.
"""

import matplotlib.pyplot as plt
import itertools
from hashlib import sha1
import numpy as np
import os
import sys
import argparse

def parse_string(s, truncate=None):
    rows = []
    row_h = []
    column_h = None
    lines = s.split('\n')
    for line in lines:
        line = line.lstrip('>')
        line = line.strip()
        if not line:
            break
        row = line.split()
        h = row.pop(0)
        if h == r'r\S':
            column_h = [int(x) for x in row]
        else:
            try:
                row_h.append(int(h))
                rows.append([float(x.replace('*', '')) for x in row])
            except ValueError as e:
                print("%s: presumably not numbers, stopping" % e)
                sys.exit(1)

    rows = np.array(rows)
    if truncate is not None:
        column_h = [x for x in column_h if x < truncate]
        rows = rows[:,:len(column_h)]

    return row_h, column_h, rows


PATTERNS = itertools.cycle(('#%s%%02x%%02x', '#%%02x%%02x%s', '#%%02x%s%%02x'))
LINESTYLES = itertools.cycle((':', '--'))

SAVE_NAME = None

def show_or_write(tag):
    if SAVE_NAME is None:
        plt.show()
    else:
        name = SAVE_NAME.replace('##', tag)
        plt.savefig(name, dpi=300)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-o', '--output', default=None,
                        help=("write graphs here, replacing ## with type "
                              "of graph (default: show in X window)"))
    parser.add_argument('files', nargs="+",
                        help="find the data in these files")
    parser.add_argument('-y', '--y-label',
                        help='label the y axis thusly')
    parser.add_argument('-t', '--truncate-x', type=int,
                        help='stop the x (-S) axis at this point')
    parser.add_argument('--no-heatmaps', action='store_true',
                        help="don't do the heatmaps")
    parser.add_argument('--variable-colour', action='store_true',
                        help="each pair of lines has a slightly different colour")

    args = parser.parse_args()

    global SAVE_NAME
    SAVE_NAME = args.output

    datasets = {}
    for fn in args.files:
        f = open(fn)
        datasets[os.path.basename(fn)] = parse_string(f.read(), args.truncate_x)
        f.close()

    if not datasets:
        print("USAGE: %s summary.txt [summary2.txt [summary3.txt [...]]]" %
              sys.argv[0])

    ax = plt.axes()
    for k, v in datasets.items():
        row_h, col_h, data = v
        n = len(data)
        if 'win1' in k:
            label = 'Windows'
            r, g, b = 0x22, 0x66, 0xff
            dr, dg, db = 60 // n, 90 // n , 0 // n
            linestyle = '--'
        else:
            label = 'Samba'
            r, g, b = 0xcc, 0, 0
            dr, dg, db = 30 // n, 135 // n, 90 // n
            linestyle = ':'

        for i, row in enumerate(data):
            S = row_h[i]

            if i == 0:
                legend = label
            else:
                legend = None

            if args.variable_colour:
                r += dr
                g += dg
                b += db

            colour = "#%02x%02x%02x" % (r & 255, g & 255, b & 255)

            ax.plot(col_h, row, linestyle, color=colour, label=legend)
            if S > 1 or len(data) > 1:
                ax.text(2, row[0], "%dx" % (S), color=colour, fontsize=10)


    ax.legend()
    if args.truncate_x:
        ax.set_xlabel('relative traffic load')
    else:
        ax.set_xlabel('attempted relative traffic load')
    if args.y_label:
        ax.set_ylabel(args.y_label)

    show_or_write('relative-load')

    if args.no_heatmaps:
        return

    for d, v in datasets.items():
        fig, ax = plt.subplots()
        rh, ch, data = v
        ax.set_title(d)
        ax.set_xlabel('S')
        ax.set_ylabel('r')
        im = ax.imshow(data)
        fig.tight_layout()
        show_or_write('%s-matrix' % d)

    if len(datasets) == 2:
        (k1, (r1, c1, d1)), (k2, (r2, c2, d2)) = datasets.items()
        diff = np.abs(d1 - d2)
        fig, ax = plt.subplots()
        im = ax.imshow(diff)
        fig.tight_layout()
        show_or_write('diff')

        scaled_diff = np.divide(diff, d2)
        fig, ax = plt.subplots()
        im = ax.imshow(scaled_diff)
        fig.tight_layout()
        show_or_write('scaled-diff')


main()
