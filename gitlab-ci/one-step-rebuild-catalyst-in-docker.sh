#!/bin/bash -e
. $(dirname $0)/sambatest.catalyst.net.nz-openrc.sh
old_umask=$(umask)
umask 077
echo $VAULT_PW > ~/.vault_password_catalyst
umask $old_umask
cd /src/
$(dirname $0)/../ansible-roles-clone-or-update.yml
$(dirname $0)/run-openstack-catalyst.sh "$@"
