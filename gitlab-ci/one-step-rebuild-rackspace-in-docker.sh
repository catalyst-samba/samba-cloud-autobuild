#!/bin/bash -xe
old_umask=$(umask)
umask 077
echo $VAULT_PW > ~/.vault_password_samba_team
umask $old_umask
cd /src/
$(dirname $0)/../ansible-roles-clone-or-update.yml
./gitlab-ci/run-rackspace-samba-team.sh "$@"
