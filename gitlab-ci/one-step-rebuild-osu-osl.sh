#!/bin/bash

. $(dirname $0)/osu-osl-openrc.sh

read -s -p 'Vault password: ' VAULT_PW
echo

docker image inspect gitlab-ci-builder > /dev/null || docker build -t gitlab-ci-builder ..

docker run -u samba -ti --mount type=bind,source="$(realpath $(dirname $0))/../",target=/src,ro gitlab-ci-builder:latest /bin/bash -c "OS_USERNAME=$OS_USERNAME OS_PASSWORD=$OS_PASSWORD VAULT_PW=$VAULT_PW /src/gitlab-ci/one-step-rebuild-osu-osl-in-docker.sh $@"
