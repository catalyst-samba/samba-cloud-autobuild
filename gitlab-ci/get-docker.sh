#!/bin/bash
set -e

sudo apt-get update
sudo apt-get install -y rng-tools
echo 0 | sudo tee /proc/sys/kernel/yama/ptrace_scope
echo 0 | sudo tee /proc/sys/kernel/perf_event_paranoid
sudo mount / -o remount,rw,nobarrier

# run fdisk -l, grep lines starts with /dev/ and followed by *
# which is the boot device.
# /dev/sda1  *     2048 20971486 20969439  10G 83 Linux
#BOOT_DEVICE=$(sudo fdisk -l | grep '^/dev/.*\*' | cut -d' ' -f1)
# rm last number to get disk, not working in sh, have to use bash
#BOOT_DISK=${BOOT_DEVICE::-1}
#sudo blockdev --setra 8192 ${BOOT_DISK}

if [ $(df -m / --output=avail | tail -n1) -gt 10240 ]; then
  sudo dd if=/dev/zero of=/samba-swap bs=1M count=6144;
  sudo mkswap /samba-swap;
  sudo swapon /samba-swap;
fi

# edit by joeg
# used by docker-machine
# docker deprecated devicemapper since 18.09
export VERSION=18.06
curl -sSL https://get.docker.com | sh -
