#!/bin/bash

if [ -z ${RAX_USERNAME+x} ]; then
    read -p 'Rackspace username: ' RAX_USERNAME
fi
if [ -z ${RAX_API_KEY+x} ]; then
  read -s -p 'Rackspace API key: ' RAX_API_KEY
fi
echo 
read -s -p 'Vault password: ' VAULT_PW
echo

docker image inspect gitlab-ci-builder > /dev/null || docker build -t gitlab-ci-builder ..

docker run -u samba -ti --mount type=bind,source="$(realpath $(dirname $0))/../",target=/src,ro gitlab-ci-builder:latest /bin/bash -c "RAX_USERNAME=$RAX_USERNAME RAX_API_KEY=$RAX_API_KEY VAULT_PW=$VAULT_PW /src/gitlab-ci/one-step-rebuild-rackspace-in-docker.sh $@"
