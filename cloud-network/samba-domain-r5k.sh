#!/bin/bash

set -xue

./samba-domain.yml -v \
    -e ENV_NAME=r5k \
    -e primary_dc_name=r5k-dc0 \
    -e samba_backup_file=$HOME/backup/samba-backup-docker-mdb-5000-max-5000.tar.bz2 \
    "$@"
